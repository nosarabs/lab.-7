package cr.ac.ucr.ecci.cql.mivoz;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions;
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager;
import com.google.firebase.ml.naturallanguage.FirebaseNaturalLanguage;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslateLanguage;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslateRemoteModel;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslator;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslatorOptions;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CODE = 1;
    // Lenguaje
    private String mLenguaje = "en-US";
    private Locale mLocale = Locale.US;
    // Texto y botones iniciales de la actividad principal
    TextView mTexto;
    // Opcion de Voz a texto
    Button mSpeechText;
    // Lista de textos encontrados
    ArrayList<String> mMatchesText;
    // Lista para mostrar los textos
    ListView mTextListView ;
    // Dialogo para mostrar la lista
    Dialog mMatchTextDialog;
    // Instancia de Texto a voz
    TextToSpeech mTextToSpeech;
    // Texto de entrada
    EditText mEditText;
    EditText mEditText2;
    // Opcion de Texto a voz
    Button mTextSpeech;

    RadioButton radioButtonEnglish;
    RadioButton radioButtonSpanish;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // componentes de la aplicacion
        mSpeechText = (Button) findViewById(R.id.buttonSpeechText);
        mTextSpeech = (Button) findViewById(R.id.buttonTextSpeech);
        mTexto = (TextView) findViewById(R.id.texto);
        mEditText = (EditText) findViewById(R.id.editText);
        mEditText2 = (EditText) findViewById(R.id.editText2);
        radioButtonEnglish = (RadioButton)findViewById(R.id.radioButtonIngles);
        radioButtonSpanish = (RadioButton)findViewById(R.id.radioButtonEspannol);

        mEditText.setHint("Escriba en ingles");
        mEditText2.setHint("Traduccion al español");
        // intent para el reconocimiento de voz
        mSpeechText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isConnected ()){
                    // intent al API de reconocimiento de voz
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE , mLenguaje);
                    startActivityForResult(intent, REQUEST_CODE);
                } else {
                    Toast.makeText(getApplicationContext(), "No hay conexión a Internet",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Set el lenguaje de texto a voz
        mTextToSpeech = new TextToSpeech(getApplicationContext(), new
                TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit (int status) {
                        mTextToSpeech.setLanguage(mLocale);
                    }
                });
        // Intente para texto a voz
        mTextSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                // lenguaje
                mTextToSpeech.setLanguage(mLocale);
                // Texto el edit
                String toSpeak = mEditText.getText().toString();Toast.makeText(getApplicationContext(), toSpeak, Toast.LENGTH_SHORT).show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    // Mayor que Lollipop
                    mTextToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, null);
                }
            }
        });

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String s2 = s.toString();

                if(radioButtonEnglish.isChecked()) {
                    translateToSpanish(s2, mEditText2);
                }else
                if(radioButtonSpanish.isChecked()){
                    translateToEnglish(s2,mEditText2);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    // evento se seleccion de idioma
    public void onRadioButtonClicked(View view) {
        // Verificar el RadioButton seleccionado
        boolean checked = ((RadioButton) view).isChecked();
        // Determinar cual RadioButton esta seleccionado
        switch(view.getId()) {
            case R.id.radioButtonIngles:
                if (checked)
                    mLenguaje = "en-US";
                mLocale = Locale.US;
                mEditText.setHint("Escriba en ingles");
                mEditText2.setHint("Traduccion al español");
                break;
            case R.id.radioButtonEspannol:
                if (checked)
                    mLenguaje = "es-ES";
                mEditText.setHint("Escriba en español");
                mEditText2.setHint("Traduccion al ingles");
                mLocale = new Locale("spa", "ESP");
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Valores de retorn del intent
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK ) {
            // Si retorna resultados el servicion de reconocimiento de voz
            // Creamos el dialogo y asignamos la lista
            mMatchTextDialog = new Dialog(MainActivity.this);
            mMatchTextDialog.setContentView(R.layout.dialog_matches_frag);
            // título del dialogo
            mMatchTextDialog.setTitle("Seleccione el texto");
            // Lista de elementos
            mTextListView = (ListView) mMatchTextDialog.findViewById(R.id.listView1);
            mMatchesText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            // Mostramos los datos en la lista
            ArrayAdapter<String> adapter = new ArrayAdapter <String>(this,
                    android.R.layout.simple_list_item_1, mMatchesText);
            mTextListView.setAdapter(adapter);
            // Asignamos el evento del clic en la lista
            mTextListView.setOnItemClickListener(new AdapterView.OnItemClickListener () {
                @Override
                public void onItemClick(AdapterView <?> parent, View view, int position, long
                        id) {
                    mTexto.setText("You have said: " + mMatchesText.get(position));
                    mEditText.setText(mMatchesText.get(position));
                    mMatchTextDialog.hide();
                }});
            mMatchTextDialog.show();
        }
    }
    // verificar conexion a internet
    public boolean isConnected (){
        ConnectivityManager cm = (ConnectivityManager) getSystemService
                (Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if(net != null && net.isAvailable() && net.isConnected()) {
            return true; } else {
            return false;
        }
    }

    public void translateToSpanish(final String text , final TextView textView){

        FirebaseModelManager modelManager = FirebaseModelManager.getInstance();

        modelManager.getDownloadedModels(FirebaseTranslateRemoteModel.class)
                .addOnSuccessListener(new OnSuccessListener<Set<FirebaseTranslateRemoteModel>>() {
                    @Override
                    public void onSuccess(Set<FirebaseTranslateRemoteModel> models) {
                        // ...
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Error.
                    }
                });

        FirebaseTranslatorOptions options =
                new FirebaseTranslatorOptions.Builder()
                        .setSourceLanguage(FirebaseTranslateLanguage.EN)
                        .setTargetLanguage(FirebaseTranslateLanguage.ES)
                        .build();
        final FirebaseTranslator englishSpanishTranslator =
                FirebaseNaturalLanguage.getInstance().getTranslator(options);

        FirebaseModelDownloadConditions conditions = new FirebaseModelDownloadConditions.Builder()
                .requireWifi()
                .build();
        englishSpanishTranslator.downloadModelIfNeeded(conditions)
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void v) {
                                // Model downloaded successfully. Okay to start translating.
                                // (Set a flag, unhide the translation UI, etc.)
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // Model couldn’t be downloaded or other internal error.
                                // ...
                            }
                        });
        englishSpanishTranslator.translate(text)
                .addOnSuccessListener(
                        new OnSuccessListener<String>() {
                            @Override
                            public void onSuccess(@NonNull String translatedText) {
                                // Translation successful.
                                textView.setText(translatedText);
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // Error.
                                // ...
                            }
                        });



    }

    public void translateToEnglish(final String text , final TextView textView){

        FirebaseModelManager modelManager = FirebaseModelManager.getInstance();

        modelManager.getDownloadedModels(FirebaseTranslateRemoteModel.class)
                .addOnSuccessListener(new OnSuccessListener<Set<FirebaseTranslateRemoteModel>>() {
                    @Override
                    public void onSuccess(Set<FirebaseTranslateRemoteModel> models) {
                        // ...
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Error.
                    }
                });

        FirebaseTranslatorOptions options =
                new FirebaseTranslatorOptions.Builder()
                        .setSourceLanguage(FirebaseTranslateLanguage.ES)
                        .setTargetLanguage(FirebaseTranslateLanguage.EN)
                        .build();
        final FirebaseTranslator spanishEnglishTranslator =
                FirebaseNaturalLanguage.getInstance().getTranslator(options);

        FirebaseModelDownloadConditions conditions = new FirebaseModelDownloadConditions.Builder()
                .requireWifi()
                .build();
        spanishEnglishTranslator.downloadModelIfNeeded(conditions)
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void v) {
                                // Model downloaded successfully. Okay to start translating.
                                // (Set a flag, unhide the translation UI, etc.)
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // Model couldn’t be downloaded or other internal error.
                                // ...
                            }
                        });
        spanishEnglishTranslator.translate(text)
                .addOnSuccessListener(
                        new OnSuccessListener<String>() {
                            @Override
                            public void onSuccess(@NonNull String translatedText) {
                                // Translation successful.
                                textView.setText(translatedText);
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // Error.
                                // ...
                            }
                        });



    }
}
